<%@page contentType="text/html;charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<HTML>
<HEAD>
<TITLE>Result</TITLE>
</HEAD>
<BODY>
<H1>Result</H1>

<jsp:useBean id="sampleIndicesServiceProxyid" scope="session" class="com.bolsadesantiago.ws.indices.IndicesServiceProxy" />
<%
if (request.getParameter("endpoint") != null && request.getParameter("endpoint").length() > 0)
sampleIndicesServiceProxyid.setEndpoint(request.getParameter("endpoint"));
%>

<%
String method = request.getParameter("method");
int methodID = 0;
if (method == null) methodID = -1;

if(methodID != -1) methodID = Integer.parseInt(method);
boolean gotMethod = false;

try {
switch (methodID){ 
case 2:
        gotMethod = true;
        java.lang.String getEndpoint2mtemp = sampleIndicesServiceProxyid.getEndpoint();
if(getEndpoint2mtemp == null){
%>
<%=getEndpoint2mtemp %>
<%
}else{
        String tempResultreturnp3 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(getEndpoint2mtemp));
        %>
        <%= tempResultreturnp3 %>
        <%
}
break;
case 5:
        gotMethod = true;
        String endpoint_0id=  request.getParameter("endpoint8");
            java.lang.String endpoint_0idTemp = null;
        if(!endpoint_0id.equals("")){
         endpoint_0idTemp  = endpoint_0id;
        }
        sampleIndicesServiceProxyid.setEndpoint(endpoint_0idTemp);
break;
case 10:
        gotMethod = true;
        com.bolsadesantiago.ws.indices.IndicesService_PortType getIndicesService_PortType10mtemp = sampleIndicesServiceProxyid.getIndicesService_PortType();
if(getIndicesService_PortType10mtemp == null){
%>
<%=getIndicesService_PortType10mtemp %>
<%
}else{
        if(getIndicesService_PortType10mtemp!= null){
        String tempreturnp11 = getIndicesService_PortType10mtemp.toString();
        %>
        <%=tempreturnp11%>
        <%
        }}
break;
case 13:
        gotMethod = true;
        String indice_1id=  request.getParameter("indice20");
            java.lang.String indice_1idTemp = null;
        if(!indice_1id.equals("")){
         indice_1idTemp  = indice_1id;
        }
        String periodo_2id=  request.getParameter("periodo22");
            java.lang.String periodo_2idTemp = null;
        if(!periodo_2id.equals("")){
         periodo_2idTemp  = periodo_2id;
        }
        String f_desde_3id=  request.getParameter("f_desde24");
            java.lang.String f_desde_3idTemp = null;
        if(!f_desde_3id.equals("")){
         f_desde_3idTemp  = f_desde_3id;
        }
        String f_hasta_4id=  request.getParameter("f_hasta26");
            java.lang.String f_hasta_4idTemp = null;
        if(!f_hasta_4id.equals("")){
         f_hasta_4idTemp  = f_hasta_4id;
        }
        com.bolsadesantiago.ws.indices.IndicesResponse consultaIndices13mtemp = sampleIndicesServiceProxyid.consultaIndices(indice_1idTemp,periodo_2idTemp,f_desde_3idTemp,f_hasta_4idTemp);
if(consultaIndices13mtemp == null){
%>
<%=consultaIndices13mtemp %>
<%
}else{
%>
<TABLE>
<TR>
<TD COLSPAN="3" ALIGN="LEFT">returnp:</TD>
<TR>
<TD WIDTH="5%"></TD>
<TD COLSPAN="2" ALIGN="LEFT">message:</TD>
<TD>
<%
if(consultaIndices13mtemp != null){
java.lang.String typemessage16 = consultaIndices13mtemp.getMessage();
        String tempResultmessage16 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typemessage16));
        %>
        <%= tempResultmessage16 %>
        <%
}%>
</TD>
<TR>
<TD WIDTH="5%"></TD>
<TD COLSPAN="2" ALIGN="LEFT">code:</TD>
<TD>
<%
if(consultaIndices13mtemp != null){
java.lang.Integer typecode18 = consultaIndices13mtemp.getCode();
        String tempResultcode18 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(typecode18));
        %>
        <%= tempResultcode18 %>
        <%
}%>
</TD>
</TABLE>
<%
}
break;
}
} catch (Exception e) { 
%>
Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.toString()) %>
Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.getMessage()) %>
<%
return;
}
if(!gotMethod){
%>
result: N/A
<%
}
%>
</BODY>
</HTML>