package com.bolsadesantiago.ws.indices;

public class IndicesServiceProxy implements com.bolsadesantiago.ws.indices.IndicesService_PortType {
  private String _endpoint = null;
  private com.bolsadesantiago.ws.indices.IndicesService_PortType indicesService_PortType = null;
  
  public IndicesServiceProxy() {
    _initIndicesServiceProxy();
  }
  
  public IndicesServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initIndicesServiceProxy();
  }
  
  private void _initIndicesServiceProxy() {
    try {
      indicesService_PortType = (new com.bolsadesantiago.ws.indices.IndicesService_ServiceLocator()).getIndicesServicePort();
      if (indicesService_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)indicesService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)indicesService_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (indicesService_PortType != null)
      ((javax.xml.rpc.Stub)indicesService_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.bolsadesantiago.ws.indices.IndicesService_PortType getIndicesService_PortType() {
    if (indicesService_PortType == null)
      _initIndicesServiceProxy();
    return indicesService_PortType;
  }
  
  public com.bolsadesantiago.ws.indices.IndicesResponse consultaIndices(java.lang.String indice, java.lang.String periodo, java.lang.String f_desde, java.lang.String f_hasta) throws java.rmi.RemoteException, com.bolsadesantiago.ws.indices.Exception{
    if (indicesService_PortType == null)
      _initIndicesServiceProxy();
    return indicesService_PortType.consultaIndices(indice, periodo, f_desde, f_hasta);
  }
  
  
}