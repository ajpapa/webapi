/**
 * IndicesItem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bolsadesantiago.ws.indices;

public class IndicesItem  implements java.io.Serializable {
    private java.lang.String fecha;

    private java.math.BigDecimal ind_act;

    private java.math.BigDecimal ind_ant;

    private java.math.BigDecimal ind_may;

    private java.math.BigDecimal ind_med;

    private java.math.BigDecimal ind_men;

    private java.math.BigDecimal ind_pro;

    private java.math.BigDecimal ind_var;

    private java.lang.String indice;

    public IndicesItem() {
    }

    public IndicesItem(
           java.lang.String fecha,
           java.math.BigDecimal ind_act,
           java.math.BigDecimal ind_ant,
           java.math.BigDecimal ind_may,
           java.math.BigDecimal ind_med,
           java.math.BigDecimal ind_men,
           java.math.BigDecimal ind_pro,
           java.math.BigDecimal ind_var,
           java.lang.String indice) {
           this.fecha = fecha;
           this.ind_act = ind_act;
           this.ind_ant = ind_ant;
           this.ind_may = ind_may;
           this.ind_med = ind_med;
           this.ind_men = ind_men;
           this.ind_pro = ind_pro;
           this.ind_var = ind_var;
           this.indice = indice;
    }


    /**
     * Gets the fecha value for this IndicesItem.
     * 
     * @return fecha
     */
    public java.lang.String getFecha() {
        return fecha;
    }


    /**
     * Sets the fecha value for this IndicesItem.
     * 
     * @param fecha
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }


    /**
     * Gets the ind_act value for this IndicesItem.
     * 
     * @return ind_act
     */
    public java.math.BigDecimal getInd_act() {
        return ind_act;
    }


    /**
     * Sets the ind_act value for this IndicesItem.
     * 
     * @param ind_act
     */
    public void setInd_act(java.math.BigDecimal ind_act) {
        this.ind_act = ind_act;
    }


    /**
     * Gets the ind_ant value for this IndicesItem.
     * 
     * @return ind_ant
     */
    public java.math.BigDecimal getInd_ant() {
        return ind_ant;
    }


    /**
     * Sets the ind_ant value for this IndicesItem.
     * 
     * @param ind_ant
     */
    public void setInd_ant(java.math.BigDecimal ind_ant) {
        this.ind_ant = ind_ant;
    }


    /**
     * Gets the ind_may value for this IndicesItem.
     * 
     * @return ind_may
     */
    public java.math.BigDecimal getInd_may() {
        return ind_may;
    }


    /**
     * Sets the ind_may value for this IndicesItem.
     * 
     * @param ind_may
     */
    public void setInd_may(java.math.BigDecimal ind_may) {
        this.ind_may = ind_may;
    }


    /**
     * Gets the ind_med value for this IndicesItem.
     * 
     * @return ind_med
     */
    public java.math.BigDecimal getInd_med() {
        return ind_med;
    }


    /**
     * Sets the ind_med value for this IndicesItem.
     * 
     * @param ind_med
     */
    public void setInd_med(java.math.BigDecimal ind_med) {
        this.ind_med = ind_med;
    }


    /**
     * Gets the ind_men value for this IndicesItem.
     * 
     * @return ind_men
     */
    public java.math.BigDecimal getInd_men() {
        return ind_men;
    }


    /**
     * Sets the ind_men value for this IndicesItem.
     * 
     * @param ind_men
     */
    public void setInd_men(java.math.BigDecimal ind_men) {
        this.ind_men = ind_men;
    }


    /**
     * Gets the ind_pro value for this IndicesItem.
     * 
     * @return ind_pro
     */
    public java.math.BigDecimal getInd_pro() {
        return ind_pro;
    }


    /**
     * Sets the ind_pro value for this IndicesItem.
     * 
     * @param ind_pro
     */
    public void setInd_pro(java.math.BigDecimal ind_pro) {
        this.ind_pro = ind_pro;
    }


    /**
     * Gets the ind_var value for this IndicesItem.
     * 
     * @return ind_var
     */
    public java.math.BigDecimal getInd_var() {
        return ind_var;
    }


    /**
     * Sets the ind_var value for this IndicesItem.
     * 
     * @param ind_var
     */
    public void setInd_var(java.math.BigDecimal ind_var) {
        this.ind_var = ind_var;
    }


    /**
     * Gets the indice value for this IndicesItem.
     * 
     * @return indice
     */
    public java.lang.String getIndice() {
        return indice;
    }


    /**
     * Sets the indice value for this IndicesItem.
     * 
     * @param indice
     */
    public void setIndice(java.lang.String indice) {
        this.indice = indice;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IndicesItem)) return false;
        IndicesItem other = (IndicesItem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fecha==null && other.getFecha()==null) || 
             (this.fecha!=null &&
              this.fecha.equals(other.getFecha()))) &&
            ((this.ind_act==null && other.getInd_act()==null) || 
             (this.ind_act!=null &&
              this.ind_act.equals(other.getInd_act()))) &&
            ((this.ind_ant==null && other.getInd_ant()==null) || 
             (this.ind_ant!=null &&
              this.ind_ant.equals(other.getInd_ant()))) &&
            ((this.ind_may==null && other.getInd_may()==null) || 
             (this.ind_may!=null &&
              this.ind_may.equals(other.getInd_may()))) &&
            ((this.ind_med==null && other.getInd_med()==null) || 
             (this.ind_med!=null &&
              this.ind_med.equals(other.getInd_med()))) &&
            ((this.ind_men==null && other.getInd_men()==null) || 
             (this.ind_men!=null &&
              this.ind_men.equals(other.getInd_men()))) &&
            ((this.ind_pro==null && other.getInd_pro()==null) || 
             (this.ind_pro!=null &&
              this.ind_pro.equals(other.getInd_pro()))) &&
            ((this.ind_var==null && other.getInd_var()==null) || 
             (this.ind_var!=null &&
              this.ind_var.equals(other.getInd_var()))) &&
            ((this.indice==null && other.getIndice()==null) || 
             (this.indice!=null &&
              this.indice.equals(other.getIndice())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFecha() != null) {
            _hashCode += getFecha().hashCode();
        }
        if (getInd_act() != null) {
            _hashCode += getInd_act().hashCode();
        }
        if (getInd_ant() != null) {
            _hashCode += getInd_ant().hashCode();
        }
        if (getInd_may() != null) {
            _hashCode += getInd_may().hashCode();
        }
        if (getInd_med() != null) {
            _hashCode += getInd_med().hashCode();
        }
        if (getInd_men() != null) {
            _hashCode += getInd_men().hashCode();
        }
        if (getInd_pro() != null) {
            _hashCode += getInd_pro().hashCode();
        }
        if (getInd_var() != null) {
            _hashCode += getInd_var().hashCode();
        }
        if (getIndice() != null) {
            _hashCode += getIndice().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IndicesItem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://indices.ws.bolsadesantiago.com/", "indicesItem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fecha");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fecha"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ind_act");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ind_act"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ind_ant");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ind_ant"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ind_may");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ind_may"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ind_med");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ind_med"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ind_men");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ind_men"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ind_pro");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ind_pro"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ind_var");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ind_var"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
