/**
 * IndicesService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bolsadesantiago.ws.indices;

public class IndicesService_ServiceLocator extends org.apache.axis.client.Service implements com.bolsadesantiago.ws.indices.IndicesService_Service {

    public IndicesService_ServiceLocator() {
    }


    public IndicesService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public IndicesService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for IndicesServicePort
    private java.lang.String IndicesServicePort_address = "http://bcstest.mybluemix.net/bcstest/IndicesService";

    public java.lang.String getIndicesServicePortAddress() {
        return IndicesServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IndicesServicePortWSDDServiceName = "IndicesServicePort";

    public java.lang.String getIndicesServicePortWSDDServiceName() {
        return IndicesServicePortWSDDServiceName;
    }

    public void setIndicesServicePortWSDDServiceName(java.lang.String name) {
        IndicesServicePortWSDDServiceName = name;
    }

    public com.bolsadesantiago.ws.indices.IndicesService_PortType getIndicesServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IndicesServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIndicesServicePort(endpoint);
    }

    public com.bolsadesantiago.ws.indices.IndicesService_PortType getIndicesServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.bolsadesantiago.ws.indices.IndicesServiceSoapBindingStub _stub = new com.bolsadesantiago.ws.indices.IndicesServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getIndicesServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIndicesServicePortEndpointAddress(java.lang.String address) {
        IndicesServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.bolsadesantiago.ws.indices.IndicesService_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.bolsadesantiago.ws.indices.IndicesServiceSoapBindingStub _stub = new com.bolsadesantiago.ws.indices.IndicesServiceSoapBindingStub(new java.net.URL(IndicesServicePort_address), this);
                _stub.setPortName(getIndicesServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("IndicesServicePort".equals(inputPortName)) {
            return getIndicesServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://indices.ws.bolsadesantiago.com/", "IndicesService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://indices.ws.bolsadesantiago.com/", "IndicesServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("IndicesServicePort".equals(portName)) {
            setIndicesServicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
