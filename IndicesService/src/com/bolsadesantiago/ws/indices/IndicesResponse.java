/**
 * IndicesResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bolsadesantiago.ws.indices;

public class IndicesResponse  implements java.io.Serializable {
    private java.lang.Integer code;

    private com.bolsadesantiago.ws.indices.IndicesItem[] indicesItem;

    private java.lang.String message;

    public IndicesResponse() {
    }

    public IndicesResponse(
           java.lang.Integer code,
           com.bolsadesantiago.ws.indices.IndicesItem[] indicesItem,
           java.lang.String message) {
           this.code = code;
           this.indicesItem = indicesItem;
           this.message = message;
    }


    /**
     * Gets the code value for this IndicesResponse.
     * 
     * @return code
     */
    public java.lang.Integer getCode() {
        return code;
    }


    /**
     * Sets the code value for this IndicesResponse.
     * 
     * @param code
     */
    public void setCode(java.lang.Integer code) {
        this.code = code;
    }


    /**
     * Gets the indicesItem value for this IndicesResponse.
     * 
     * @return indicesItem
     */
    public com.bolsadesantiago.ws.indices.IndicesItem[] getIndicesItem() {
        return indicesItem;
    }


    /**
     * Sets the indicesItem value for this IndicesResponse.
     * 
     * @param indicesItem
     */
    public void setIndicesItem(com.bolsadesantiago.ws.indices.IndicesItem[] indicesItem) {
        this.indicesItem = indicesItem;
    }

    public com.bolsadesantiago.ws.indices.IndicesItem getIndicesItem(int i) {
        return this.indicesItem[i];
    }

    public void setIndicesItem(int i, com.bolsadesantiago.ws.indices.IndicesItem _value) {
        this.indicesItem[i] = _value;
    }


    /**
     * Gets the message value for this IndicesResponse.
     * 
     * @return message
     */
    public java.lang.String getMessage() {
        return message;
    }


    /**
     * Sets the message value for this IndicesResponse.
     * 
     * @param message
     */
    public void setMessage(java.lang.String message) {
        this.message = message;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IndicesResponse)) return false;
        IndicesResponse other = (IndicesResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.code==null && other.getCode()==null) || 
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.indicesItem==null && other.getIndicesItem()==null) || 
             (this.indicesItem!=null &&
              java.util.Arrays.equals(this.indicesItem, other.getIndicesItem()))) &&
            ((this.message==null && other.getMessage()==null) || 
             (this.message!=null &&
              this.message.equals(other.getMessage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getIndicesItem() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getIndicesItem());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getIndicesItem(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMessage() != null) {
            _hashCode += getMessage().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IndicesResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://indices.ws.bolsadesantiago.com/", "indicesResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indicesItem");
        elemField.setXmlName(new javax.xml.namespace.QName("", "indicesItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://indices.ws.bolsadesantiago.com/", "indicesItem"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("message");
        elemField.setXmlName(new javax.xml.namespace.QName("", "message"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
