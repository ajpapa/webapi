/**
 * IndicesService_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bolsadesantiago.ws.indices;

public interface IndicesService_PortType extends java.rmi.Remote {
    public com.bolsadesantiago.ws.indices.IndicesResponse consultaIndices(java.lang.String indice, java.lang.String periodo, java.lang.String f_desde, java.lang.String f_hasta) throws java.rmi.RemoteException, com.bolsadesantiago.ws.indices.Exception;
}
